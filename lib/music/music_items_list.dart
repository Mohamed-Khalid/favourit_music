import 'package:myfavorite_music_app/components/custom_music_items.dart';

List<CustomMusicItem> musicItems = const [
  CustomMusicItem(
    name: "أغنية معك ع الموت",
    musicName: "aElmoot.mp3",
  ),
  CustomMusicItem(
    name: "أغنية أحلي صبية بشوف",
    musicName: "AhlaSabiyeh.m4a",
  ),
  CustomMusicItem(
    name: "أغنية أن الاوان (وانت بعيد )",
    musicName: "AnnAlawan_.m4a",
  ),
  CustomMusicItem(
    name: "أغنية من الدنيا دي انا بتمناك",
    musicName: "batmanak.mp3",
  ),
  CustomMusicItem(
    name: "أغنية عينك بعيني ",
    musicName: "enakfeEaney.mp3",
  ),
  CustomMusicItem(
    name: "أغنية انت الحب اللي اتمنيته ",
    musicName: "entaElhob.m4a",
  ),
  CustomMusicItem(
    name: "أغنية اتنسيت كأني مجيت ",
    musicName: "Etnaset.mp3",
  ),
  CustomMusicItem(
    name: "أغنية طول عمري بحلم ",
    musicName: "gamalk.mp3",
  ),
  CustomMusicItem(
    name: "أغنية اللي خدتني مني ",
    musicName: "hamootWa.mp3",
  ),
  CustomMusicItem(
    name: "أغنية كده بحبها مش عارف اسمها ",
    musicName: "HerYer.m4a",
  ),
  CustomMusicItem(
    name: "أغنية انا لما بحب بحن",
    musicName: "LammaBheb.m4a",
  ),
  CustomMusicItem(
    name: "أغنية لا تروح ابعيد عني",
    musicName: "laTroubead.mp3",
  ),
  CustomMusicItem(
    name: "أغنية بعيونه بضرب ميت مثلا)",
    musicName: "llely.mp3",
  ),
  CustomMusicItem(
    name: "أغنية نسيانك صعب اكيد ",
    musicName: "Nesynak.mp3",
  ),
  CustomMusicItem(
    name: "أغنية اللي تاخدك من اسي",
    musicName: "takhadak.mp3",
  ),
  CustomMusicItem(
    name: "أغنية قابلتك امتي ",
    musicName: "AbeltkEmta.m4a",
  ),
  CustomMusicItem(
    name: "أغنية بيهم كلهم ",
    musicName: "BeehomKolohom.m4a",
  ),
  CustomMusicItem(
    name: "أغنية بيني وبينك ",
    musicName: "BeinyWeBeinak.m4a",
  ),
  CustomMusicItem(
    name: "أغنية بتوصفني بتكسفني",
    musicName: "Btewsefni.m4a",
  ),
  CustomMusicItem(
    name: "أغنية انت اختيار",
    musicName: "EntaEkhtyar.m4a",
  ),
  CustomMusicItem(
    name: "أغنية قلهالي ",
    musicName: "Alhali.m4a",
  ),
  CustomMusicItem(
    name: "أغنية عيونه لما قابلوني ",
    musicName: "Oyouno_Lama.m4a",
  ),
];
