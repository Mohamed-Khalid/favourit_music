import 'package:flutter/material.dart';
import 'package:myfavorite_music_app/home/home_screen.dart';
import 'package:myfavorite_music_app/music/music_screen.dart';
import 'package:myfavorite_music_app/quran/quran_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const HomeScreen(),
      routes: {
        "quran": (context) => const QuranScreen(),
        "music": (context) => const MusicScreen(),
      },
    );
  }
}
