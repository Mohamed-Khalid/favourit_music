import 'package:myfavorite_music_app/components/custom_quran_item.dart';

List<CustomQuramItem> quranItems = const [
  CustomQuramItem(
    name: "صورة الأنبياء",
    musicName: "anbyaa.mp3",
  ),
  CustomQuramItem(
    name: "صورة الفرقان",
    musicName: "forqaan.mp3",
  ),
  CustomQuramItem(
    name: "صورة الجاثية",
    musicName: "gathyaa.mp3",
  ),
  CustomQuramItem(
    name: "صورة الحجرات",
    musicName: "hograat.mp3",
  ),
  CustomQuramItem(
    name: "صورة الكهف",
    musicName: "kahaf.mp3",
  ),
  CustomQuramItem(
    name: "صورة المسد",
    musicName: "masad.mp3",
  ),
  CustomQuramItem(
    name: "صورة الملك",
    musicName: "molq.mp3",
  ),
  CustomQuramItem(
    name: "صورة الرحمن",
    musicName: "rahmaan.mp3",
  ),
  CustomQuramItem(
    name: "ضورة يس",
    musicName: "yaseen.mp3",
  ),
  CustomQuramItem(
    name: "صورة يوسف",
    musicName: "yossuf.mp3",
  ),
  CustomQuramItem(
    name: "أية وذا النون",
    musicName: "WaZannon.m4a",
  ),
];
