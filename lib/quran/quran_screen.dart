import 'package:flutter/material.dart';
import 'package:myfavorite_music_app/quran/quran_items_list.dart';

class QuranScreen extends StatelessWidget {
  const QuranScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          backgroundColor: const Color(0xffcf17e8),
          appBar: AppBar(
            backgroundColor: const Color.fromARGB(255, 160, 64, 177),
            leading: const Icon(
              Icons.home,
              size: 30,
            ),
            //  backgroundColor: Colors.,
            title: const Text(
              "القرأن المفضل لدي",
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
            ),
          ),
          body: ListView.separated(
            itemBuilder: ((context, index) => quranItems[index]),
            separatorBuilder: ((context, index) => Container(
                  margin: const EdgeInsets.only(top: 20, bottom: 20),
                  width: double.infinity,
                  height: 1.5,
                  color: Colors.white,
                )),
            itemCount: quranItems.length,
          )),
    );
  }
}
