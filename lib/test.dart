import 'package:flutter/material.dart';

class Test extends StatelessWidget {
  const Test({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 150, bottom: 20),
            child: Text(
              "Sign In",
              style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
            ),
          ),
          TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(40),

                    )
                    ),
          )
        ],
      ),
    );
  }
}
