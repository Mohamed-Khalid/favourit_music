import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class CustomMusicItem extends StatefulWidget {
  const CustomMusicItem(
      {super.key, required this.name, required this.musicName});
  final String name;
  final String musicName;

  @override
  State<CustomMusicItem> createState() => _CustomMusicItem();
}

class _CustomMusicItem extends State<CustomMusicItem> {
  bool isSelected = false;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Text(
          //varٍ
          widget.name,
          style: const TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w500,
          ),
        ),
        const Spacer(),
        isSelected == true
            ? IconButton(
                onPressed: () {
                  setState(() {
                    isSelected = !isSelected;
                  });
                  // AudioCache player = AudioCache(prefix: "assets/qraan/");
                  // player.play(widget.musicName);
                },
                icon: const Icon(
                  Icons.pause,
                  size: 40,
                ),
              )
            : IconButton(
                onPressed: () {
                  setState(() {
                    isSelected = !isSelected;
                  });
                  AudioCache player = AudioCache(prefix: "assets/music/");
                  player.play(widget.musicName);
                },
                icon: const Icon(
                  Icons.play_arrow,
                  size: 40,
                ),
              )
      ],
    );
  }
}
